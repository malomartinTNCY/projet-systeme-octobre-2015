TestPerso: testBeMa.o libBeMa.so
	export LD_LIBRARY_PATH=.
	gcc -fPIC testBeMa.o -L. -lBeMa -o TestPerso -Wall
	
testBeMa.o: testBeMa.c
	gcc -fPIC -c testBeMa.c -Wall

mem.o: mem.c
	gcc -fPIC -c mem.c -Wall

libBeMa.so: mem.o
	gcc -fPIC -shared -o libBeMa.so mem.o -Wall
	
clean:
	rm *.o
	rm TestPerso BenchTest
	rm drawMemory.txt freeList.txt allocatedList.txt

BenchTest: testmem.o libBeMa.so
	export LD_LIBRARY_PATH=.
	gcc -fPIC testmem.o -L. -lBeMa -o BenchTest -Wall

testmem.o: testmem.c
	gcc -fPIC -c testmem.c -Wall