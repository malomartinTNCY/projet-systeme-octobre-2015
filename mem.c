#include "mem.h"

node_t *headA = NULL;
node_t *headF = NULL;	//a chained list for allocated blocks and one for free blocks
int Mem_Init(int sizeOfRegion)
{
	int length;

	if((sizeOfRegion % getpagesize() == 0))
	{
		length = sizeOfRegion;
	}
	else 
	{
		length = (sizeOfRegion / getpagesize() + 1) * getpagesize();
	}
	headF = mmap(NULL, length, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANON, -1, 0);
	headF->size = length - sizeof(node_t);
	headF->next = NULL;
	headA=NULL;
	return 0;	
}

int Mem_IsValid(void *ptr)
{
	if (whereInList(ptr, headA)!=NULL)
		return 1;	//if function does not return NULL, ptr belongs to headA's list.
	else
		return -1;	
}
		

int Mem_GetSize(void *ptr)
{
	node_t* ptr_node = whereInList(ptr, headA);
	if (ptr_node!=NULL)	//if function does not return NULL, ptr belongs to headA's list.
		return ptr_node->size;	// then return the size of the block
	else
		return -1;
}

void *Mem_Alloc(int size)	//best fit
{
	if(headF == NULL)
		Mem_Init(INI_SIZE);
	node_t buffer;
	node_t* cursor = headF;		//along the list of free blocks, the real pointed node is the next of cursor
	node_t* best = &buffer;
	best->next = headF;	//the real best block is the next of the node kept in memory
	bool hasFound = (best->next->size > size);
	while(cursor->next!=NULL)
	{
		if(cursor->next->size >= size  &&  cursor->next->size < best->next->size)		//if the free chunk is large enougth and better than best
		{
			best=cursor;
			hasFound=true;
		}
		else
		{
			cursor=cursor->next;
		}
	}


	// here if hasFound best->next point on the block that worst fit the demanded size
	// else allocation is not possible
	if (hasFound)
	{
		/*
		With best fit aproach we ofen have very small remaining blocks.
		Is it worthy to keep it in our list of free block
		knowing that the longer this list is, the more time allocations will take ? 
		The more this test will be true, the less allocations will cost.
		This approach amplifies benefits and drawbacks of best fit approach : 
		If there is not enougth allocations and free of various sizes,
		choosing the best of an uniform set is not worthy.
		*/


		if (best->next->size - size < sizeof(node_t) + sizeof(char))	//if if remain not enougth place for another allocation
		{
			cursor=best->next->next; //remind the successor of the removed block

			best->next->next=headA;
			headA = best->next;	//change pointers to move the block at the head of allocated blocks' list
			
			if(best->next == headF)	//specific case : first bloc fit
				headF=cursor;
			else
				best->next=cursor;
		}
		else //to split the block we need a new node_t object 
		{
			cursor =(node_t*)( (void*)best->next + size + sizeof(node_t) ); //new node farther.
			//without cast cursor increments of (size+sizeof(node_t)*sizeof(node_t))
			cursor->size = best->next->size - size - sizeof(node_t); //with smaller size
			cursor->next = best->next->next;	//and same next

			best->next->next=headA;
			best->next->size = size;
			headA = best->next;

			if(best->next == headF)	//specific case : first bloc fit
				headF=cursor;
			else
				best->next=cursor;
			

		}
		return (void*)headA + sizeof(node_t);	
	}
	else
	{
		return NULL;

	}
}

int Mem_Free(void *ptr)						//Free stuff
{
	node_t* ptr_node = (node_t*) ptr;

	node_t buffer;
	buffer.next = headA;
	node_t* cursor = &buffer;

	bool hasFound = false;

	while ( (cursor->next != NULL) && (!hasFound) )
	{

		if ( ((void*)cursor->next + sizeof(node_t) <= ptr) && ( ptr < (void*)cursor->next + sizeof(node_t) + cursor->next->size ) )	//if ptr belong to the cursor's block
		{	
			hasFound = true;
		}
		else
			cursor = cursor->next;
	}
	if (hasFound)
	{
		if (ptr_node -1 == cursor -> next) //if we have to free the entire block
		{
			if (cursor->next == headA)	//if we free the head of the list, cursor point on a local varible
			{
				headA = NULL;	//then we have to modifie headF directly

				cursor->next->next = headF;	//we put the block at the head of the free blocks list
				headF = cursor->next;	//and set the new head of the free blocks list
			}
			else
			{
				buffer.next = cursor->next->next;	//remind the next of the block to free
				
				cursor->next->next = headF;	//we put the block at the head of the free blocks list
				headF = cursor->next;	//and set the new head of the free blocks list

				cursor->next = buffer.next;	//collapse previous and next
			}

			
			
			
		}
		else	//if we have to free just the end of the block
		{
			buffer.size = ptr - (void*)cursor->next - sizeof(node_t);	//we ballance the automatic division by the size of the pointed struct
			ptr_node->size = cursor->next->size - buffer.size - sizeof(node_t);	//crete a real node at ptr
			ptr_node->next = headF;	//we put the block at the head of the free blocks list
			headF = ptr_node;	//and set the new head of the free blocks list

			cursor->next->size = buffer.size;	//the allocated block is smaller
		}

		cursor = headF;
		
		while(cursor->next != NULL)
		{
				if((void*)cursor->next + cursor->next->size + sizeof(node_t) == (void*)headF)
				{
					cursor->next->size += headF->size + sizeof(node_t);
					headF = headF->next;
				}
				if((void*)headF + headF->size + sizeof(node_t) == (void*)cursor->next)
				{
					headF->size += cursor->next->size + sizeof(node_t);

					cursor->next = cursor->next->next;
				}
				else cursor = cursor->next;
		}

		return 0;
	}
	else
	{
		return -1;
	}

	/*
	node_t buffer;
	buffer.next=headA;

	node_t* cursor = &buffer;	//real cursor on cursor->next then we have ton initialize cursor on a node out of the list

	while (cursor->next!=NULL)	//in the list given by [head]
	{

		if (((void*)cursor->next + sizeof(node_t) <= ptr) && (ptr < (void*) cursor->next + cursor->next->size + sizeof(node_t)) )	//if ptr belong to the cursor's block
		{
			if(cursor == &buffer)
			{
				headA = NULL;
				cursor->next->next = headF;
				headF = cursor->next;
			}
			else
			{
				buffer.next=cursor->next->next;
				cursor->next->next=headF;
				headF=cursor->next;
				cursor->next = buffer.next;
			}
			return 0; //the block has been freed
		}
		cursor=cursor->next;
	}
	return -1; //ptr isn't an allocated block
	*/
}





///USEFUL FUNCTIONS///

node_t* whereInList(void *ptr, node_t *head) //ptr is infact a void* but we have to compare it to cursor, a node_t*
{
	node_t* ptr_node = (node_t*) ptr; 
	node_t *cursor = head;
	while (cursor!=NULL)	//in the list given by [head]
	{
		if ( (cursor - ptr_node <= 0) && ( (ptr_node - cursor) * sizeof(node_t) < cursor->size + sizeof(node_t) ) )	//if ptr belong to the cursor's block
		{
			return cursor; //return the address of the block
		}
		cursor=cursor->next;
	}
	return NULL; //else return NULL
}

