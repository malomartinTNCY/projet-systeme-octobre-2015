#ifndef MEM
#define MEM

#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/mman.h>
#include <stdbool.h>

#define INI_SIZE 16384 // default sizeOfRegion

int Mem_Init(int sizeOfRegion);
void *Mem_Alloc(int size);
int Mem_Free(void *ptr);
int Mem_IsValid(void *ptr);
int Mem_GetSize(void *ptr);


typedef struct node_t			//Node of the chained list of free chunck
{
	int size;
	struct node_t* next;
}node_t;

node_t* whereInList(void *ptr, node_t *head);

union ptr_t
{
	void* v; node_t* n; char* c;
} ptr_t;

#endif