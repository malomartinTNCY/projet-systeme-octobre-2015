#include "mem.h"
#include <unistd.h>



extern node_t *headA;
extern node_t *headF;


void drawMemory(void* beginning)
{
	void* i;
	node_t *allocatedBlock=NULL;
	node_t *freeBlock=NULL;
	FILE* file = fopen("drawMemory.txt", "w");
	for (i=beginning; i<beginning+INI_SIZE; i+= sizeof(char))
	{
		allocatedBlock = whereInList(i,headA);
		freeBlock = whereInList(i,headF);
		if (allocatedBlock!=NULL)
		{
			if(i==(void*)allocatedBlock)
			{
				fprintf(file, "%p**NEW-BLOCK**\n",i);
				fprintf(file, "%p*SIZE:%d-\n",i, allocatedBlock->size);
				fprintf(file, "%p*NEXT:%p-\n",i,allocatedBlock->next);
				fprintf(file, "%p**END:%p**\n",i,i + allocatedBlock->size + sizeof(node_t) - 1); //without +1 the first address of the next contiguous block
				i+= sizeof(node_t) - sizeof(char); //to avoid double increment
			}

			else if (i==(void*)allocatedBlock + allocatedBlock->size + sizeof(node_t))
			{
				fprintf(file, "%p***AAAAAAAAA***\n",i);
			}
				else
				{
					fprintf(file, "%p***aaaaaaaaa***\n",i);
				}
		}
		else if (freeBlock!=NULL)
		{
			fprintf(file, "%p***fffffffff***\n",i);
		}
		else
		{
			fprintf(file, "%p***ooooooooo***\n",i);
		}
	}
	fclose(file);
}

void writeList()
{
	node_t* cursor = headF;
	FILE* file = fopen("freeList.txt", "w");
	while (cursor != NULL)
	{
		fprintf(file,"(%p|%d|%p)\n",cursor, cursor->size, cursor->next);
		cursor=cursor->next;
	}
	fclose(file);

	cursor = headA;
	file = fopen("allocatedList.txt", "w");
	while (cursor != NULL)
	{
		
		fprintf(file,"(%p|%d|%p)\n",cursor, cursor->size, cursor->next);
		cursor=cursor->next;
	}
	fclose(file);
}
int main()
{
	Mem_Init(INI_SIZE);
	void * beginning = (void*) headF;
	
	void* p1 = Mem_Alloc(60); 
	void* p2 = Mem_Alloc(100);
	int	t1f = Mem_Free(p1);	//usual free
	printf("usual free %d\n", t1f);
	int t2x = Mem_Free(p2-20);	//usual fail free (memory before p2 not alocated as we free p1)
	printf("usual fail free %d\n", t2x);
	int t2p = Mem_Free(p2+30);	//partial free
	printf("usual partial free %d\n", t2p);
	int t2xx = Mem_Free(p2+35);	//usual fail free
	printf("usual fail free %d\n", t2xx);
	int t2f = Mem_Free(p2); //usual free 
	printf("usual free %d\n", t2f);
	Mem_Alloc(900);	

	drawMemory(beginning); //visual representations of the memory
	writeList();
	return 1;
}

